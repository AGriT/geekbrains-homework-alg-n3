#include <stdio.h>

int nIsNumberSimple(int nSimple)
{
	int i = 2;

	if ((nSimple == 2) || (nSimple == 3))
		return 1;

	if (nSimple % 2 == 0)
		return 0;

	for (i = 3; i < nSimple / 2; i += 2)
		if (nSimple % i == 0)
			return 0;
	
	return 1;
}

int main()
{
	int nIsSimple = 0;
	int nFlag = 0;

	do
	{
		printf("Insert Number. Must be higher 1\n");
		scanf("%d", &nIsSimple);
	} while (nIsSimple <= 1);

	nFlag = nIsNumberSimple(nIsSimple);

	printf("Inserted number is %s Simple\n", nFlag ? "" : "NOT");
	
	return 0;
}